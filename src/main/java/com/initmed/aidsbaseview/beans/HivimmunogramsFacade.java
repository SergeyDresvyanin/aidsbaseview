/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.initmed.aidsbaseview.beans;

import com.initmed.aidsbaseview.entity.Hivimmunograms;
import com.initmed.aidsbaseview.entity.Persons;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

/**
 *
 * @author sd199
 */
@Stateless
public class HivimmunogramsFacade extends AbstractFacade<Hivimmunograms> {

    @PersistenceContext(unitName = "AIB_baseViewPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public HivimmunogramsFacade() {
        super(Hivimmunograms.class);
    }

    public List<Hivimmunograms> getLastCd4(String from, String dateTo, boolean lowCd4, Persons ps) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Hivimmunograms> q = cb.createQuery(Hivimmunograms.class);
        Root<Hivimmunograms> c = q.distinct(true).from(Hivimmunograms.class);
        List<Predicate> predicates = new ArrayList<>();
        predicates.add(cb.equal(c.get("refpersons"), ps));
        predicates.add(cb.isNotNull(c.get("cd4")));
        if (from != null && from.length() > 1) {
            predicates.add(cb.greaterThanOrEqualTo(c.get("takingdate"), sdf.parse(from)));
        }
        if (lowCd4) {
            predicates.add(cb.lessThan(c.get("cd4"), 350));
        }
        if (dateTo != null && dateTo.length() > 1) {
            predicates.add(cb.lessThanOrEqualTo(c.get("takingdate"), sdf.parse(dateTo)));
        }

        q.where(predicates.toArray(new Predicate[]{}));
        q.orderBy(cb.desc(c.get("takingdate")));
        return em.createQuery(q).getResultList();
    }
}
