/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.initmed.aidsbaseview.beans;

import com.initmed.aidsbaseview.entity.Hiv;
import com.initmed.aidsbaseview.entity.Persons;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

/**
 *
 * @author sd199
 */
@Stateless
public class HivFacade extends AbstractFacade<Hiv> {

    @PersistenceContext(unitName = "AIB_baseViewPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public HivFacade() {
        super(Hiv.class);
    }

    public List<Hiv> getHivsByIDCode(int ID) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Hiv> q = cb.createQuery(Hiv.class);
        Root<Hiv> c = q.from(Hiv.class);

        List<Predicate> predicates = new ArrayList<>();
        predicates.add(cb.equal(c.get("registryno"), ID));
        q.where(predicates.toArray(new Predicate[]{}));
        return em.createQuery(q).getResultList();
    }
}
