/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.initmed.aidsbaseview.beans;

import com.initmed.aidsbaseview.entity.Hiv;
import com.initmed.aidsbaseview.entity.Hivimmunograms;
import com.initmed.aidsbaseview.entity.Persons;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

/**
 *
 * @author sd199
 */
@Stateless
public class PersonsFacade extends AbstractFacade<Persons> {

    @PersistenceContext(unitName = "AIB_baseViewPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public PersonsFacade() {
        super(Persons.class);

    }

    public List<Persons> findByFilter(String filter, String live, String dateFrom, String dateTo, boolean lowcd4, int pos, int rowCount) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Persons> q = cb.createQuery(Persons.class);
        Root<Persons> c = q.distinct(true).from(Persons.class);
        List<Predicate> predicates = new ArrayList<>();
        if (filter != null) {
            predicates.add(cb.like((Expression) c.get("nameI"), "%" + filter.toUpperCase() + "%"));
        }
        Join<Hivimmunograms, Persons> hivimmunograms;
        if (lowcd4) {
            hivimmunograms = c.join("hivimmunogramsCollection", JoinType.INNER);
            predicates.add(cb.lessThan(hivimmunograms.get("cd4"), 350));
        } else {
            hivimmunograms = c.join("hivimmunogramsCollection", JoinType.LEFT);
        }

        if (dateFrom != null && dateFrom.length() > 1) {
            predicates.add(cb.greaterThanOrEqualTo(hivimmunograms.get("takingdate"), sdf.parse(dateFrom)));
        }
        if (dateTo != null && dateTo.length() > 1) {
            predicates.add(cb.lessThanOrEqualTo(hivimmunograms.get("takingdate"), sdf.parse(dateTo)));
        }
        if (live != null) {
            predicates.add(cb.or(cb.like(cb.upper((Expression) c.get("addressLiv")), "%" + live.toUpperCase() + "%"), cb.like(cb.upper((Expression) c.get("addressReg")), "%" + live.toUpperCase() + "%")));
        }

        q.where(predicates.toArray(new Predicate[]{}));
        return em.createQuery(q)
                .setFirstResult(pos).setMaxResults(rowCount).getResultList();
    }

    public Persons getPersonByID(int id) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Persons> q = cb.createQuery(Persons.class);
        Root<Persons> c = q.from(Persons.class);
        List<Predicate> predicates = new ArrayList<>();
        predicates.add(cb.equal(c.get("id"), id));
        q.where(predicates.toArray(new Predicate[]{}));
        return em.createQuery(q).getResultList().get(0);
    }

    public List<Persons> findByIDCode(String idCode) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Persons> q = cb.createQuery(Persons.class);
        Root<Persons> c = q.from(Persons.class);
        List<Predicate> predicates = new ArrayList<>();
        predicates.add(cb.equal(c.get("ambulatorycardno"), idCode));
        q.where(predicates.toArray(new Predicate[]{}));
        return em.createQuery(q).getResultList();
    }
}
