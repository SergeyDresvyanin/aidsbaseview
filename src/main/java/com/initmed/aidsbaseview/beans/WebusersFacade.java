/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.initmed.aidsbaseview.beans;

import com.initmed.aidsbaseview.entity.Persons;
import com.initmed.aidsbaseview.entity.Webusers;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

/**
 *
 * @author Sergey_Dresvyanin
 */
@Stateless
public class WebusersFacade extends AbstractFacade<Webusers> {

    @PersistenceContext(unitName = "AIB_baseViewPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public WebusersFacade() {
        super(Webusers.class);
    }

    public Webusers getUser(String login) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Webusers> q = cb.createQuery(Webusers.class);
        Root<Webusers> c = q.from(Webusers.class);
        List<Predicate> predicates = new ArrayList<>();
        predicates.add(cb.equal((Expression) c.get("username"), login));
        q.where(predicates.toArray(new Predicate[]{}));
        return em.createQuery(q).getResultList().get(0);
    }
}