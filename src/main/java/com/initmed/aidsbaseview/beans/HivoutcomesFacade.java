/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.initmed.aidsbaseview.beans;

import com.initmed.aidsbaseview.entity.Hivoutcomes;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author sd199
 */
@Stateless
public class HivoutcomesFacade extends AbstractFacade<Hivoutcomes> {

    @PersistenceContext(unitName = "AIB_baseViewPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public HivoutcomesFacade() {
        super(Hivoutcomes.class);
    }
    
}
