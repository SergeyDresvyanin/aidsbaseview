/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.initmed.aidsbaseview.beans;

import com.initmed.aidsbaseview.entity.Persons;
import com.initmed.aidsbaseview.entity.VPersonsDiagnoses;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

/**
 *
 * @author sd199
 */
@Stateless
public class VPersonsDiagnosesFacade extends AbstractFacade<VPersonsDiagnoses> {

    @PersistenceContext(unitName = "AIB_baseViewPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public VPersonsDiagnosesFacade() {
        super(VPersonsDiagnoses.class);
    }
     public List<VPersonsDiagnoses> getByID(int id) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<VPersonsDiagnoses> q = cb.createQuery(VPersonsDiagnoses.class);
        Root<VPersonsDiagnoses> c = q.from(VPersonsDiagnoses.class);
        List<Predicate> predicates = new ArrayList<Predicate>();
        predicates.add(cb.equal(c.get("id"), id));
        q.where(predicates.toArray(new Predicate[]{}));
        return em.createQuery(q).getResultList();
     }
}
