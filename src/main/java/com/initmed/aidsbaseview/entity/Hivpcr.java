/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.initmed.aidsbaseview.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author sd199
 */
@Entity
@Table(name = "HIVPCR")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Hivpcr.findAll", query = "SELECT h FROM Hivpcr h"),
    @NamedQuery(name = "Hivpcr.findById", query = "SELECT h FROM Hivpcr h WHERE h.id = :id"),
    @NamedQuery(name = "Hivpcr.findByTakingdate", query = "SELECT h FROM Hivpcr h WHERE h.takingdate = :takingdate"),
    @NamedQuery(name = "Hivpcr.findByMakingdate", query = "SELECT h FROM Hivpcr h WHERE h.makingdate = :makingdate"),
    @NamedQuery(name = "Hivpcr.findByAnswerdate", query = "SELECT h FROM Hivpcr h WHERE h.answerdate = :answerdate"),
    @NamedQuery(name = "Hivpcr.findBySampleno", query = "SELECT h FROM Hivpcr h WHERE h.sampleno = :sampleno"),
    @NamedQuery(name = "Hivpcr.findByAnalysisno", query = "SELECT h FROM Hivpcr h WHERE h.analysisno = :analysisno"),
    @NamedQuery(name = "Hivpcr.findByViralload", query = "SELECT h FROM Hivpcr h WHERE h.viralload = :viralload"),
    @NamedQuery(name = "Hivpcr.findByIsbelowsensivity", query = "SELECT h FROM Hivpcr h WHERE h.isbelowsensivity = :isbelowsensivity")})
public class Hivpcr implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Column(name = "TAKINGDATE")
    @Temporal(TemporalType.DATE)
    private Date takingdate;
    @Column(name = "MAKINGDATE")
    @Temporal(TemporalType.DATE)
    private Date makingdate;
    @Column(name = "ANSWERDATE")
    @Temporal(TemporalType.DATE)
    private Date answerdate;
    @Column(name = "SAMPLENO")
    private Integer sampleno;
    @Column(name = "ANALYSISNO")
    private Integer analysisno;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "VIRALLOAD")
    private Double viralload;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ISBELOWSENSIVITY")
    private short isbelowsensivity;
    @JoinColumn(name = "REFHIVANALYSISRESULTS", referencedColumnName = "ID")
    @ManyToOne
    private Hivanalysisresults refhivanalysisresults;
    @JoinColumn(name = "REFPERSONS", referencedColumnName = "ID")
    @ManyToOne(optional = false)
    private Persons refpersons;

    public Hivpcr() {
    }

    public Hivpcr(Integer id) {
        this.id = id;
    }

    public Hivpcr(Integer id, Date takingdate, short isbelowsensivity) {
        this.id = id;
        this.takingdate = takingdate;
        this.isbelowsensivity = isbelowsensivity;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getTakingdate() {
        return takingdate;
    }

    public void setTakingdate(Date takingdate) {
        this.takingdate = takingdate;
    }

    public Date getMakingdate() {
        return makingdate;
    }

    public void setMakingdate(Date makingdate) {
        this.makingdate = makingdate;
    }

    public Date getAnswerdate() {
        return answerdate;
    }

    public void setAnswerdate(Date answerdate) {
        this.answerdate = answerdate;
    }

    public Integer getSampleno() {
        return sampleno;
    }

    public void setSampleno(Integer sampleno) {
        this.sampleno = sampleno;
    }

    public Integer getAnalysisno() {
        return analysisno;
    }

    public void setAnalysisno(Integer analysisno) {
        this.analysisno = analysisno;
    }

    public Double getViralload() {
        return viralload;
    }

    public void setViralload(Double viralload) {
        this.viralload = viralload;
    }

    public short getIsbelowsensivity() {
        return isbelowsensivity;
    }

    public void setIsbelowsensivity(short isbelowsensivity) {
        this.isbelowsensivity = isbelowsensivity;
    }

    public Hivanalysisresults getRefhivanalysisresults() {
        return refhivanalysisresults;
    }

    public void setRefhivanalysisresults(Hivanalysisresults refhivanalysisresults) {
        this.refhivanalysisresults = refhivanalysisresults;
    }

    public Persons getRefpersons() {
        return refpersons;
    }

    public void setRefpersons(Persons refpersons) {
        this.refpersons = refpersons;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Hivpcr)) {
            return false;
        }
        Hivpcr other = (Hivpcr) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.Hivpcr[ id=" + id + " ]";
    }

}
