/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.initmed.aidsbaseview.entity;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author sd199
 */
@Entity
@Table(name = "HIVINFECTIONWAYS")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Hivinfectionways.findAll", query = "SELECT h FROM Hivinfectionways h")
    , @NamedQuery(name = "Hivinfectionways.findById", query = "SELECT h FROM Hivinfectionways h WHERE h.id = :id")
    , @NamedQuery(name = "Hivinfectionways.findByName", query = "SELECT h FROM Hivinfectionways h WHERE h.name = :name")})
public class Hivinfectionways implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "NAME")
    private String name;
    @OneToMany(mappedBy = "refhivinfectionways")
    private Collection<Hiv> hivCollection;

    public Hivinfectionways() {
    }

    public Hivinfectionways(Integer id) {
        this.id = id;
    }

    public Hivinfectionways(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @XmlTransient
    public Collection<Hiv> getHivCollection() {
        return hivCollection;
    }

    public void setHivCollection(Collection<Hiv> hivCollection) {
        this.hivCollection = hivCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Hivinfectionways)) {
            return false;
        }
        Hivinfectionways other = (Hivinfectionways) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.Hivinfectionways[ id=" + id + " ]";
    }
    
}
