/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.initmed.aidsbaseview.entity;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author sd199
 */
@Entity
@Table(name = "HIVART")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Hivart.findAll", query = "SELECT h FROM Hivart h")
    , @NamedQuery(name = "Hivart.findById", query = "SELECT h FROM Hivart h WHERE h.id = :id")
    , @NamedQuery(name = "Hivart.findByStartdate", query = "SELECT h FROM Hivart h WHERE h.startdate = :startdate")
    , @NamedQuery(name = "Hivart.findByEnddate", query = "SELECT h FROM Hivart h WHERE h.enddate = :enddate")
    , @NamedQuery(name = "Hivart.findByStartreason", query = "SELECT h FROM Hivart h WHERE h.startreason = :startreason")
    , @NamedQuery(name = "Hivart.findByEndreason", query = "SELECT h FROM Hivart h WHERE h.endreason = :endreason")})
public class Hivart implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Column(name = "STARTDATE")
    @Temporal(TemporalType.DATE)
    private Date startdate;
    @Column(name = "ENDDATE")
    @Temporal(TemporalType.DATE)
    private Date enddate;
    @Size(max = 255)
    @Column(name = "STARTREASON")
    private String startreason;
    @Size(max = 255)
    @Column(name = "ENDREASON")
    private String endreason;
    @JoinColumn(name = "REFHIVARTOUTCOMES", referencedColumnName = "ID")
    @ManyToOne
    private Hivartoutcomes refhivartoutcomes;
    @JoinColumn(name = "REFPERSONS", referencedColumnName = "ID")
    @ManyToOne(optional = false)
    private Persons refpersons;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "refhivart")
    private Collection<Hivartdrugs> hivartdrugsCollection;

    public Hivart() {
    }

    public Hivart(Integer id) {
        this.id = id;
    }

    public Hivart(Integer id, Date startdate) {
        this.id = id;
        this.startdate = startdate;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getStartdate() {
        return startdate;
    }

    public void setStartdate(Date startdate) {
        this.startdate = startdate;
    }

    public Date getEnddate() {
        return enddate;
    }

    public void setEnddate(Date enddate) {
        this.enddate = enddate;
    }

    public String getStartreason() {
        return startreason;
    }

    public void setStartreason(String startreason) {
        this.startreason = startreason;
    }

    public String getEndreason() {
        return endreason;
    }

    public void setEndreason(String endreason) {
        this.endreason = endreason;
    }

    public Hivartoutcomes getRefhivartoutcomes() {
        return refhivartoutcomes;
    }

    public void setRefhivartoutcomes(Hivartoutcomes refhivartoutcomes) {
        this.refhivartoutcomes = refhivartoutcomes;
    }

    public Persons getRefpersons() {
        return refpersons;
    }

    public void setRefpersons(Persons refpersons) {
        this.refpersons = refpersons;
    }

    @XmlTransient
    public Collection<Hivartdrugs> getHivartdrugsCollection() {
        return hivartdrugsCollection;
    }

    public void setHivartdrugsCollection(Collection<Hivartdrugs> hivartdrugsCollection) {
        this.hivartdrugsCollection = hivartdrugsCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Hivart)) {
            return false;
        }
        Hivart other = (Hivart) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.Hivart[ id=" + id + " ]";
    }
    
}
