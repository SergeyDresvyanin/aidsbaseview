/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.initmed.aidsbaseview.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author sd199
 */
@Entity
@Table(name = "HIVARTDRUGS")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Hivartdrugs.findAll", query = "SELECT h FROM Hivartdrugs h")
    , @NamedQuery(name = "Hivartdrugs.findById", query = "SELECT h FROM Hivartdrugs h WHERE h.id = :id")
    , @NamedQuery(name = "Hivartdrugs.findByDescription", query = "SELECT h FROM Hivartdrugs h WHERE h.description = :description")})
public class Hivartdrugs implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Integer id;
    @Size(max = 255)
    @Column(name = "DESCRIPTION")
    private String description;
    @JoinColumn(name = "REFDRUGS", referencedColumnName = "ID")
    @ManyToOne(optional = false)
    private Drugs refdrugs;
    @JoinColumn(name = "REFHIVART", referencedColumnName = "ID")
    @ManyToOne(optional = false)
    private Hivart refhivart;

    public Hivartdrugs() {
    }

    public Hivartdrugs(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Drugs getRefdrugs() {
        return refdrugs;
    }

    public void setRefdrugs(Drugs refdrugs) {
        this.refdrugs = refdrugs;
    }

    public Hivart getRefhivart() {
        return refhivart;
    }

    public void setRefhivart(Hivart refhivart) {
        this.refhivart = refhivart;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Hivartdrugs)) {
            return false;
        }
        Hivartdrugs other = (Hivartdrugs) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.Hivartdrugs[ id=" + id + " ]";
    }
    
}
