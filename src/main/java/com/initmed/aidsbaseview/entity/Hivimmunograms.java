/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.initmed.aidsbaseview.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author sd199
 */
@Entity
@Table(name = "HIVIMMUNOGRAMS")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Hivimmunograms.findAll", query = "SELECT h FROM Hivimmunograms h")
    , @NamedQuery(name = "Hivimmunograms.findById", query = "SELECT h FROM Hivimmunograms h WHERE h.id = :id")
    , @NamedQuery(name = "Hivimmunograms.findByTakingdate", query = "SELECT h FROM Hivimmunograms h WHERE h.takingdate = :takingdate")
    , @NamedQuery(name = "Hivimmunograms.findByMakingdate", query = "SELECT h FROM Hivimmunograms h WHERE h.makingdate = :makingdate")
    , @NamedQuery(name = "Hivimmunograms.findByAnswerdate", query = "SELECT h FROM Hivimmunograms h WHERE h.answerdate = :answerdate")
    , @NamedQuery(name = "Hivimmunograms.findBySampleno", query = "SELECT h FROM Hivimmunograms h WHERE h.sampleno = :sampleno")
    , @NamedQuery(name = "Hivimmunograms.findByAnalysisno", query = "SELECT h FROM Hivimmunograms h WHERE h.analysisno = :analysisno")
    , @NamedQuery(name = "Hivimmunograms.findByCd4Percent", query = "SELECT h FROM Hivimmunograms h WHERE h.cd4Percent = :cd4Percent")
    , @NamedQuery(name = "Hivimmunograms.findByCd8Percent", query = "SELECT h FROM Hivimmunograms h WHERE h.cd8Percent = :cd8Percent")
    , @NamedQuery(name = "Hivimmunograms.findByCd4", query = "SELECT h FROM Hivimmunograms h WHERE h.cd4 = :cd4")
    , @NamedQuery(name = "Hivimmunograms.findByCd8", query = "SELECT h FROM Hivimmunograms h WHERE h.cd8 = :cd8")
    , @NamedQuery(name = "Hivimmunograms.findByIri", query = "SELECT h FROM Hivimmunograms h WHERE h.iri = :iri")
    , @NamedQuery(name = "Hivimmunograms.findByCd3", query = "SELECT h FROM Hivimmunograms h WHERE h.cd3 = :cd3")
    , @NamedQuery(name = "Hivimmunograms.findByCd3Percent", query = "SELECT h FROM Hivimmunograms h WHERE h.cd3Percent = :cd3Percent")})
public class Hivimmunograms implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Column(name = "TAKINGDATE")
    @Temporal(TemporalType.DATE)
    private Date takingdate;
    @Column(name = "MAKINGDATE")
    @Temporal(TemporalType.DATE)
    private Date makingdate;
    @Column(name = "ANSWERDATE")
    @Temporal(TemporalType.DATE)
    private Date answerdate;
    @Column(name = "SAMPLENO")
    private Integer sampleno;
    @Column(name = "ANALYSISNO")
    private Integer analysisno;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "CD4_PERCENT")
    private Double cd4Percent;
    @Column(name = "CD8_PERCENT")
    private Double cd8Percent;
    @Column(name = "CD4")
    private Double cd4;
    @Column(name = "CD8")
    private Double cd8;
    @Column(name = "IRI")
    private Double iri;
    @Column(name = "CD3")
    private Double cd3;
    @Column(name = "CD3_PERCENT")
    private Double cd3Percent;
    @JoinColumn(name = "REFPERSONS", referencedColumnName = "ID")
    @ManyToOne(optional = false)
    private Persons refpersons;

    public Hivimmunograms() {
    }

    public Hivimmunograms(Integer id) {
        this.id = id;
    }

    public Hivimmunograms(Integer id, Date takingdate) {
        this.id = id;
        this.takingdate = takingdate;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getTakingdate() {
        return takingdate;
    }

    public void setTakingdate(Date takingdate) {
        this.takingdate = takingdate;
    }

    public Date getMakingdate() {
        return makingdate;
    }

    public void setMakingdate(Date makingdate) {
        this.makingdate = makingdate;
    }

    public Date getAnswerdate() {
        return answerdate;
    }

    public void setAnswerdate(Date answerdate) {
        this.answerdate = answerdate;
    }

    public Integer getSampleno() {
        return sampleno;
    }

    public void setSampleno(Integer sampleno) {
        this.sampleno = sampleno;
    }

    public Integer getAnalysisno() {
        return analysisno;
    }

    public void setAnalysisno(Integer analysisno) {
        this.analysisno = analysisno;
    }

    public Double getCd4Percent() {
        return cd4Percent;
    }

    public void setCd4Percent(Double cd4Percent) {
        this.cd4Percent = cd4Percent;
    }

    public Double getCd8Percent() {
        return cd8Percent;
    }

    public void setCd8Percent(Double cd8Percent) {
        this.cd8Percent = cd8Percent;
    }

    public Double getCd4() {
        return cd4;
    }

    public void setCd4(Double cd4) {
        this.cd4 = cd4;
    }

    public Double getCd8() {
        return cd8;
    }

    public void setCd8(Double cd8) {
        this.cd8 = cd8;
    }

    public Double getIri() {
        return iri;
    }

    public void setIri(Double iri) {
        this.iri = iri;
    }

    public Double getCd3() {
        return cd3;
    }

    public void setCd3(Double cd3) {
        this.cd3 = cd3;
    }

    public Double getCd3Percent() {
        return cd3Percent;
    }

    public void setCd3Percent(Double cd3Percent) {
        this.cd3Percent = cd3Percent;
    }

    public Persons getRefpersons() {
        return refpersons;
    }

    public void setRefpersons(Persons refpersons) {
        this.refpersons = refpersons;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Hivimmunograms)) {
            return false;
        }
        Hivimmunograms other = (Hivimmunograms) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.Hivimmunograms[ id=" + id + " ]";
    }
    
}
