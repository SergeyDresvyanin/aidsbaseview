/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.initmed.aidsbaseview.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author sd199
 */
@Entity
@Table(name = "V_PERSONS_DIAGNOSES")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "VPersonsDiagnoses.findAll", query = "SELECT v FROM VPersonsDiagnoses v")
    , @NamedQuery(name = "VPersonsDiagnoses.findById", query = "SELECT v FROM VPersonsDiagnoses v WHERE v.id = :id")
    , @NamedQuery(name = "VPersonsDiagnoses.findByRefpersons", query = "SELECT v FROM VPersonsDiagnoses v WHERE v.refpersons = :refpersons")
    , @NamedQuery(name = "VPersonsDiagnoses.findByLastdate", query = "SELECT v FROM VPersonsDiagnoses v WHERE v.lastdate = :lastdate")
    , @NamedQuery(name = "VPersonsDiagnoses.findByClassifiercode", query = "SELECT v FROM VPersonsDiagnoses v WHERE v.classifiercode = :classifiercode")
    , @NamedQuery(name = "VPersonsDiagnoses.findByName", query = "SELECT v FROM VPersonsDiagnoses v WHERE v.name = :name")})
public class VPersonsDiagnoses implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Column(name = "ID")
    private Integer id;

    @Column(name = "REFPERSONS")
    private Integer refpersons;
    @Column(name = "LASTDATE")
    @Temporal(TemporalType.DATE)
    private Date lastdate;
    @Size(max = 10)
    @Column(name = "CLASSIFIERCODE")
    private String classifiercode;
    @Size(max = 255)
    @Column(name = "NAME")
    private String name;

    public VPersonsDiagnoses() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getRefpersons() {
        return refpersons;
    }

    public void setRefpersons(Integer refpersons) {
        this.refpersons = refpersons;
    }

    public Date getLastdate() {
        return lastdate;
    }

    public void setLastdate(Date lastdate) {
        this.lastdate = lastdate;
    }

    public String getClassifiercode() {
        return classifiercode;
    }

    public void setClassifiercode(String classifiercode) {
        this.classifiercode = classifiercode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}