/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.initmed.aidsbaseview.entity;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author sd199
 */
@Entity
@Table(name = "HIVPHASES")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Hivphases.findAll", query = "SELECT h FROM Hivphases h")
    , @NamedQuery(name = "Hivphases.findById", query = "SELECT h FROM Hivphases h WHERE h.id = :id")
    , @NamedQuery(name = "Hivphases.findByName", query = "SELECT h FROM Hivphases h WHERE h.name = :name")})
public class Hivphases implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "NAME")
    private String name;
    @OneToMany(mappedBy = "refhivphases")
    private Collection<Hivstages> hivstagesCollection;

    public Hivphases() {
    }

    public Hivphases(Integer id) {
        this.id = id;
    }

    public Hivphases(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @XmlTransient
    public Collection<Hivstages> getHivstagesCollection() {
        return hivstagesCollection;
    }

    public void setHivstagesCollection(Collection<Hivstages> hivstagesCollection) {
        this.hivstagesCollection = hivstagesCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Hivphases)) {
            return false;
        }
        Hivphases other = (Hivphases) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.Hivphases[ id=" + id + " ]";
    }
    
}
