/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.initmed.aidsbaseview.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author sd199
 */
@Entity
@Table(name = "HIV")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Hiv.findAll", query = "SELECT h FROM Hiv h")
    , @NamedQuery(name = "Hiv.findById", query = "SELECT h FROM Hiv h WHERE h.id = :id")
    , @NamedQuery(name = "Hiv.findByDetectiondate", query = "SELECT h FROM Hiv h WHERE h.detectiondate = :detectiondate")
    , @NamedQuery(name = "Hiv.findByRegistryno", query = "SELECT h FROM Hiv h WHERE h.registryno = :registryno")
    , @NamedQuery(name = "Hiv.findByRegistrydate", query = "SELECT h FROM Hiv h WHERE h.registrydate = :registrydate")
    , @NamedQuery(name = "Hiv.findByUnregistrydate", query = "SELECT h FROM Hiv h WHERE h.unregistrydate = :unregistrydate")
    , @NamedQuery(name = "Hiv.findByRefhivoutcomes", query = "SELECT h FROM Hiv h WHERE h.refhivoutcomes = :refhivoutcomes")
    , @NamedQuery(name = "Hiv.findByIspostmortem", query = "SELECT h FROM Hiv h WHERE h.ispostmortem = :ispostmortem")
    , @NamedQuery(name = "Hiv.findByIsconcomitanttreatment", query = "SELECT h FROM Hiv h WHERE h.isconcomitanttreatment = :isconcomitanttreatment")
    , @NamedQuery(name = "Hiv.findByIsautopsy", query = "SELECT h FROM Hiv h WHERE h.isautopsy = :isautopsy")
    , @NamedQuery(name = "Hiv.findByIsfirsttime", query = "SELECT h FROM Hiv h WHERE h.isfirsttime = :isfirsttime")
    , @NamedQuery(name = "Hiv.findByIsotherorg", query = "SELECT h FROM Hiv h WHERE h.isotherorg = :isotherorg")
    , @NamedQuery(name = "Hiv.findByIsufsin", query = "SELECT h FROM Hiv h WHERE h.isufsin = :isufsin")
    , @NamedQuery(name = "Hiv.findByIsotherregion", query = "SELECT h FROM Hiv h WHERE h.isotherregion = :isotherregion")
    , @NamedQuery(name = "Hiv.findByIsforeign", query = "SELECT h FROM Hiv h WHERE h.isforeign = :isforeign")
    , @NamedQuery(name = "Hiv.findByInfectiondate", query = "SELECT h FROM Hiv h WHERE h.infectiondate = :infectiondate")})
public class Hiv implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Integer id;
    @Column(name = "DETECTIONDATE")
    @Temporal(TemporalType.DATE)
    private Date detectiondate;
    @Column(name = "REGISTRYNO")
    private Integer registryno;
    @Basic(optional = false)
    @NotNull
    @Column(name = "REGISTRYDATE")
    @Temporal(TemporalType.DATE)
    private Date registrydate;
    @Column(name = "UNREGISTRYDATE")
    @Temporal(TemporalType.DATE)
    private Date unregistrydate;
    @Column(name = "REFHIVOUTCOMES")
    private Integer refhivoutcomes;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ISPOSTMORTEM")
    private short ispostmortem;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ISCONCOMITANTTREATMENT")
    private short isconcomitanttreatment;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ISAUTOPSY")
    private short isautopsy;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ISFIRSTTIME")
    private short isfirsttime;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ISOTHERORG")
    private short isotherorg;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ISUFSIN")
    private short isufsin;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ISOTHERREGION")
    private short isotherregion;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ISFOREIGN")
    private short isforeign;
    @Size(max = 20)
    @Column(name = "INFECTIONDATE")
    private String infectiondate;
    @JoinColumn(name = "REFHIVINFECTIONWAYS", referencedColumnName = "ID")
    @ManyToOne
    private Hivinfectionways refhivinfectionways;
    @JoinColumn(name = "REFPERSONS", referencedColumnName = "ID")
    @ManyToOne(optional = false)
    private Persons refpersons;

    public Hiv() {
    }

    public Hiv(Integer id) {
        this.id = id;
    }

    public Hiv(Integer id, Date registrydate, short ispostmortem, short isconcomitanttreatment, short isautopsy, short isfirsttime, short isotherorg, short isufsin, short isotherregion, short isforeign) {
        this.id = id;
        this.registrydate = registrydate;
        this.ispostmortem = ispostmortem;
        this.isconcomitanttreatment = isconcomitanttreatment;
        this.isautopsy = isautopsy;
        this.isfirsttime = isfirsttime;
        this.isotherorg = isotherorg;
        this.isufsin = isufsin;
        this.isotherregion = isotherregion;
        this.isforeign = isforeign;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getDetectiondate() {
        return detectiondate;
    }

    public void setDetectiondate(Date detectiondate) {
        this.detectiondate = detectiondate;
    }

    public Integer getRegistryno() {
        return registryno;
    }

    public void setRegistryno(Integer registryno) {
        this.registryno = registryno;
    }

    public Date getRegistrydate() {
        return registrydate;
    }

    public void setRegistrydate(Date registrydate) {
        this.registrydate = registrydate;
    }

    public Date getUnregistrydate() {
        return unregistrydate;
    }

    public void setUnregistrydate(Date unregistrydate) {
        this.unregistrydate = unregistrydate;
    }

    public Integer getRefhivoutcomes() {
        return refhivoutcomes;
    }

    public void setRefhivoutcomes(Integer refhivoutcomes) {
        this.refhivoutcomes = refhivoutcomes;
    }

    public short getIspostmortem() {
        return ispostmortem;
    }

    public void setIspostmortem(short ispostmortem) {
        this.ispostmortem = ispostmortem;
    }

    public short getIsconcomitanttreatment() {
        return isconcomitanttreatment;
    }

    public void setIsconcomitanttreatment(short isconcomitanttreatment) {
        this.isconcomitanttreatment = isconcomitanttreatment;
    }

    public short getIsautopsy() {
        return isautopsy;
    }

    public void setIsautopsy(short isautopsy) {
        this.isautopsy = isautopsy;
    }

    public short getIsfirsttime() {
        return isfirsttime;
    }

    public void setIsfirsttime(short isfirsttime) {
        this.isfirsttime = isfirsttime;
    }

    public short getIsotherorg() {
        return isotherorg;
    }

    public void setIsotherorg(short isotherorg) {
        this.isotherorg = isotherorg;
    }

    public short getIsufsin() {
        return isufsin;
    }

    public void setIsufsin(short isufsin) {
        this.isufsin = isufsin;
    }

    public short getIsotherregion() {
        return isotherregion;
    }

    public void setIsotherregion(short isotherregion) {
        this.isotherregion = isotherregion;
    }

    public short getIsforeign() {
        return isforeign;
    }

    public void setIsforeign(short isforeign) {
        this.isforeign = isforeign;
    }

    public String getInfectiondate() {
        return infectiondate;
    }

    public void setInfectiondate(String infectiondate) {
        this.infectiondate = infectiondate;
    }

    public Hivinfectionways getRefhivinfectionways() {
        return refhivinfectionways;
    }

    public void setRefhivinfectionways(Hivinfectionways refhivinfectionways) {
        this.refhivinfectionways = refhivinfectionways;
    }

    public Persons getRefpersons() {
        return refpersons;
    }

    public void setRefpersons(Persons refpersons) {
        this.refpersons = refpersons;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Hiv)) {
            return false;
        }
        Hiv other = (Hiv) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.Hiv[ id=" + id + " ]";
    }
    
}
