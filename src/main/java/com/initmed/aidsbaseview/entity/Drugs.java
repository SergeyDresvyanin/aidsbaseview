/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.initmed.aidsbaseview.entity;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author sd199
 */
@Entity
@Table(name = "DRUGS")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Drugs.findAll", query = "SELECT d FROM Drugs d")
    , @NamedQuery(name = "Drugs.findById", query = "SELECT d FROM Drugs d WHERE d.id = :id")
    , @NamedQuery(name = "Drugs.findByDosage", query = "SELECT d FROM Drugs d WHERE d.dosage = :dosage")
    , @NamedQuery(name = "Drugs.findByQuantity", query = "SELECT d FROM Drugs d WHERE d.quantity = :quantity")
    , @NamedQuery(name = "Drugs.findByDatefrom", query = "SELECT d FROM Drugs d WHERE d.datefrom = :datefrom")
    , @NamedQuery(name = "Drugs.findByDatetill", query = "SELECT d FROM Drugs d WHERE d.datetill = :datetill")
    , @NamedQuery(name = "Drugs.findByReuselimit", query = "SELECT d FROM Drugs d WHERE d.reuselimit = :reuselimit")
    , @NamedQuery(name = "Drugs.findByCode", query = "SELECT d FROM Drugs d WHERE d.code = :code")
    , @NamedQuery(name = "Drugs.findByDescr", query = "SELECT d FROM Drugs d WHERE d.descr = :descr")})
public class Drugs implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Integer id;
    @Size(max = 255)
    @Column(name = "DOSAGE")
    private String dosage;
    @Basic(optional = false)
    @NotNull
    @Column(name = "QUANTITY")
    private int quantity;
    @Column(name = "DATEFROM")
    @Temporal(TemporalType.DATE)
    private Date datefrom;
    @Column(name = "DATETILL")
    @Temporal(TemporalType.DATE)
    private Date datetill;
    @Column(name = "REUSELIMIT")
    private Integer reuselimit;
    @Size(max = 20)
    @Column(name = "CODE")
    private String code;
    @Size(max = 1000)
    @Column(name = "DESCR")
    private String descr;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "refdrugs")
    private Collection<Hivartdrugs> hivartdrugsCollection;

    public Drugs() {
    }

    public Drugs(Integer id) {
        this.id = id;
    }

    public Drugs(Integer id, int quantity) {
        this.id = id;
        this.quantity = quantity;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDosage() {
        return dosage;
    }

    public void setDosage(String dosage) {
        this.dosage = dosage;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public Date getDatefrom() {
        return datefrom;
    }

    public void setDatefrom(Date datefrom) {
        this.datefrom = datefrom;
    }

    public Date getDatetill() {
        return datetill;
    }

    public void setDatetill(Date datetill) {
        this.datetill = datetill;
    }

    public Integer getReuselimit() {
        return reuselimit;
    }

    public void setReuselimit(Integer reuselimit) {
        this.reuselimit = reuselimit;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescr() {
        return descr;
    }

    public void setDescr(String descr) {
        this.descr = descr;
    }

    @XmlTransient
    public Collection<Hivartdrugs> getHivartdrugsCollection() {
        return hivartdrugsCollection;
    }

    public void setHivartdrugsCollection(Collection<Hivartdrugs> hivartdrugsCollection) {
        this.hivartdrugsCollection = hivartdrugsCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Drugs)) {
            return false;
        }
        Drugs other = (Drugs) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.Drugs[ id=" + id + " ]";
    }
    
}
