/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.initmed.aidsbaseview.entity;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author sd199
 */
@Entity
@Table(name = "HIVANALYSISTYPES")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Hivanalysistypes.findAll", query = "SELECT h FROM Hivanalysistypes h")
    , @NamedQuery(name = "Hivanalysistypes.findById", query = "SELECT h FROM Hivanalysistypes h WHERE h.id = :id")
    , @NamedQuery(name = "Hivanalysistypes.findByName", query = "SELECT h FROM Hivanalysistypes h WHERE h.name = :name")
    , @NamedQuery(name = "Hivanalysistypes.findByIshiv", query = "SELECT h FROM Hivanalysistypes h WHERE h.ishiv = :ishiv")
    , @NamedQuery(name = "Hivanalysistypes.findByOrderno", query = "SELECT h FROM Hivanalysistypes h WHERE h.orderno = :orderno")})
public class Hivanalysistypes implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "NAME")
    private String name;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ISHIV")
    private short ishiv;
    @Column(name = "ORDERNO")
    private Integer orderno;
    @OneToMany(mappedBy = "atype")
    private Collection<Hivanalysisresults> hivanalysisresultsCollection;

    public Hivanalysistypes() {
    }

    public Hivanalysistypes(Integer id) {
        this.id = id;
    }

    public Hivanalysistypes(Integer id, String name, short ishiv) {
        this.id = id;
        this.name = name;
        this.ishiv = ishiv;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public short getIshiv() {
        return ishiv;
    }

    public void setIshiv(short ishiv) {
        this.ishiv = ishiv;
    }

    public Integer getOrderno() {
        return orderno;
    }

    public void setOrderno(Integer orderno) {
        this.orderno = orderno;
    }

    @XmlTransient
    public Collection<Hivanalysisresults> getHivanalysisresultsCollection() {
        return hivanalysisresultsCollection;
    }

    public void setHivanalysisresultsCollection(Collection<Hivanalysisresults> hivanalysisresultsCollection) {
        this.hivanalysisresultsCollection = hivanalysisresultsCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Hivanalysistypes)) {
            return false;
        }
        Hivanalysistypes other = (Hivanalysistypes) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.Hivanalysistypes[ id=" + id + " ]";
    }
    
}
