/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.initmed.aidsbaseview.entity;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author sd199
 */
@Entity
@Table(name = "PERSONS")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Persons.findAll", query = "SELECT p FROM Persons p"),
    @NamedQuery(name = "Persons.findById", query = "SELECT p FROM Persons p WHERE p.id = :id"),
    @NamedQuery(name = "Persons.findByName", query = "SELECT p FROM Persons p WHERE p.name = :name"),
    @NamedQuery(name = "Persons.findByNameI", query = "SELECT p FROM Persons p WHERE p.nameI = :nameI"),
    @NamedQuery(name = "Persons.findByBirthday", query = "SELECT p FROM Persons p WHERE p.birthday = :birthday"),
    @NamedQuery(name = "Persons.findByDateofdeath", query = "SELECT p FROM Persons p WHERE p.dateofdeath = :dateofdeath"),
    @NamedQuery(name = "Persons.findByAmbulatorycardno", query = "SELECT p FROM Persons p WHERE p.ambulatorycardno = :ambulatorycardno"),
    @NamedQuery(name = "Persons.findBySocialno", query = "SELECT p FROM Persons p WHERE p.socialno = :socialno"),
    @NamedQuery(name = "Persons.findByPersonalno", query = "SELECT p FROM Persons p WHERE p.personalno = :personalno"),
    @NamedQuery(name = "Persons.findByPersonalcertificateno", query = "SELECT p FROM Persons p WHERE p.personalcertificateno = :personalcertificateno"),
    @NamedQuery(name = "Persons.findByHomephoneno", query = "SELECT p FROM Persons p WHERE p.homephoneno = :homephoneno"),
    @NamedQuery(name = "Persons.findByWorkphoneno", query = "SELECT p FROM Persons p WHERE p.workphoneno = :workphoneno"),
    @NamedQuery(name = "Persons.findByMobilephoneno", query = "SELECT p FROM Persons p WHERE p.mobilephoneno = :mobilephoneno"),
    @NamedQuery(name = "Persons.findByHousenoReg", query = "SELECT p FROM Persons p WHERE p.housenoReg = :housenoReg"),
    @NamedQuery(name = "Persons.findByBuildingReg", query = "SELECT p FROM Persons p WHERE p.buildingReg = :buildingReg"),
    @NamedQuery(name = "Persons.findByRoomnoReg", query = "SELECT p FROM Persons p WHERE p.roomnoReg = :roomnoReg"),
    @NamedQuery(name = "Persons.findByNoteforaddressReg", query = "SELECT p FROM Persons p WHERE p.noteforaddressReg = :noteforaddressReg"),
    @NamedQuery(name = "Persons.findByAddressReg", query = "SELECT p FROM Persons p WHERE p.addressReg = :addressReg"),
    @NamedQuery(name = "Persons.findByHousenoLiv", query = "SELECT p FROM Persons p WHERE p.housenoLiv = :housenoLiv"),
    @NamedQuery(name = "Persons.findByBuildingLiv", query = "SELECT p FROM Persons p WHERE p.buildingLiv = :buildingLiv"),
    @NamedQuery(name = "Persons.findByRoomnoLiv", query = "SELECT p FROM Persons p WHERE p.roomnoLiv = :roomnoLiv"),
    @NamedQuery(name = "Persons.findByNoteforaddressLiv", query = "SELECT p FROM Persons p WHERE p.noteforaddressLiv = :noteforaddressLiv"),
    @NamedQuery(name = "Persons.findByAddressLiv", query = "SELECT p FROM Persons p WHERE p.addressLiv = :addressLiv"),
    @NamedQuery(name = "Persons.findByDocumentno", query = "SELECT p FROM Persons p WHERE p.documentno = :documentno"),
    @NamedQuery(name = "Persons.findByDocdeliver", query = "SELECT p FROM Persons p WHERE p.docdeliver = :docdeliver"),
    @NamedQuery(name = "Persons.findByDeliverydate", query = "SELECT p FROM Persons p WHERE p.deliverydate = :deliverydate"),
    @NamedQuery(name = "Persons.findByIsdisabilityremoved", query = "SELECT p FROM Persons p WHERE p.isdisabilityremoved = :isdisabilityremoved"),
    @NamedQuery(name = "Persons.findByIstreatedasmandatoryinsured", query = "SELECT p FROM Persons p WHERE p.istreatedasmandatoryinsured = :istreatedasmandatoryinsured"),
    @NamedQuery(name = "Persons.findByIssocialservicesabandoned", query = "SELECT p FROM Persons p WHERE p.issocialservicesabandoned = :issocialservicesabandoned"),
    @NamedQuery(name = "Persons.findByIsorganizedchild", query = "SELECT p FROM Persons p WHERE p.isorganizedchild = :isorganizedchild"),
    @NamedQuery(name = "Persons.findByIsunorganizedchild", query = "SELECT p FROM Persons p WHERE p.isunorganizedchild = :isunorganizedchild"),
    @NamedQuery(name = "Persons.findByIsworking", query = "SELECT p FROM Persons p WHERE p.isworking = :isworking"),
    @NamedQuery(name = "Persons.findByIsbomzh", query = "SELECT p FROM Persons p WHERE p.isbomzh = :isbomzh"),
    @NamedQuery(name = "Persons.findByIsstudent", query = "SELECT p FROM Persons p WHERE p.isstudent = :isstudent"),
    @NamedQuery(name = "Persons.findByIspensioner", query = "SELECT p FROM Persons p WHERE p.ispensioner = :ispensioner"),
    @NamedQuery(name = "Persons.findByIssoldier", query = "SELECT p FROM Persons p WHERE p.issoldier = :issoldier"),
    @NamedQuery(name = "Persons.findByIssoldierfamilymember", query = "SELECT p FROM Persons p WHERE p.issoldierfamilymember = :issoldierfamilymember"),
    @NamedQuery(name = "Persons.findByIsbudgetworker", query = "SELECT p FROM Persons p WHERE p.isbudgetworker = :isbudgetworker"),
    @NamedQuery(name = "Persons.findByIshardwork", query = "SELECT p FROM Persons p WHERE p.ishardwork = :ishardwork"),
    @NamedQuery(name = "Persons.findByProfession", query = "SELECT p FROM Persons p WHERE p.profession = :profession"),
    @NamedQuery(name = "Persons.findByWorkposition", query = "SELECT p FROM Persons p WHERE p.workposition = :workposition"),
    @NamedQuery(name = "Persons.findByFiredate", query = "SELECT p FROM Persons p WHERE p.firedate = :firedate"),
    @NamedQuery(name = "Persons.findByIsfromdistrict", query = "SELECT p FROM Persons p WHERE p.isfromdistrict = :isfromdistrict"),
    @NamedQuery(name = "Persons.findByWorkplace", query = "SELECT p FROM Persons p WHERE p.workplace = :workplace"),
    @NamedQuery(name = "Persons.findByArdoctorid", query = "SELECT p FROM Persons p WHERE p.ardoctorid = :ardoctorid"),
    @NamedQuery(name = "Persons.findByArpatientid", query = "SELECT p FROM Persons p WHERE p.arpatientid = :arpatientid"),
    @NamedQuery(name = "Persons.findByIsanonym", query = "SELECT p FROM Persons p WHERE p.isanonym = :isanonym")})
public class Persons implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 84)
    @Column(name = "NAME")
    private String name;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 84)
    @Column(name = "NAME_I")
    private String nameI;
    @Column(name = "BIRTHDAY")
    @Temporal(TemporalType.DATE)
    private Date birthday;
    @Column(name = "DATEOFDEATH")
    @Temporal(TemporalType.DATE)
    private Date dateofdeath;
    @Column(name = "AMBULATORYCARDNO")
    private String ambulatorycardno;
    @Size(max = 15)
    @Column(name = "SOCIALNO")
    private String socialno;
    @Size(max = 15)
    @Column(name = "PERSONALNO")
    private String personalno;
    @Size(max = 15)
    @Column(name = "PERSONALCERTIFICATENO")
    private String personalcertificateno;
    @Size(max = 15)
    @Column(name = "HOMEPHONENO")
    private String homephoneno;
    @Size(max = 15)
    @Column(name = "WORKPHONENO")
    private String workphoneno;
    @Size(max = 15)
    @Column(name = "MOBILEPHONENO")
    private String mobilephoneno;
    @Size(max = 50)
    @Column(name = "HOUSENO_REG")
    private String housenoReg;
    @Size(max = 50)
    @Column(name = "BUILDING_REG")
    private String buildingReg;
    @Size(max = 50)
    @Column(name = "ROOMNO_REG")
    private String roomnoReg;
    @Size(max = 250)
    @Column(name = "NOTEFORADDRESS_REG")
    private String noteforaddressReg;
    @Size(max = 400)
    @Column(name = "ADDRESS_REG")
    private String addressReg;
    @Size(max = 50)
    @Column(name = "HOUSENO_LIV")
    private String housenoLiv;
    @Size(max = 50)
    @Column(name = "BUILDING_LIV")
    private String buildingLiv;
    @Size(max = 50)
    @Column(name = "ROOMNO_LIV")
    private String roomnoLiv;
    @Size(max = 250)
    @Column(name = "NOTEFORADDRESS_LIV")
    private String noteforaddressLiv;
    @Size(max = 400)
    @Column(name = "ADDRESS_LIV")
    private String addressLiv;
    @Size(max = 20)
    @Column(name = "DOCUMENTNO")
    private String documentno;
    @Size(max = 100)
    @Column(name = "DOCDELIVER")
    private String docdeliver;
    @Column(name = "DELIVERYDATE")
    @Temporal(TemporalType.DATE)
    private Date deliverydate;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ISDISABILITYREMOVED")
    private short isdisabilityremoved;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ISTREATEDASMANDATORYINSURED")
    private short istreatedasmandatoryinsured;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ISSOCIALSERVICESABANDONED")
    private short issocialservicesabandoned;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ISORGANIZEDCHILD")
    private short isorganizedchild;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ISUNORGANIZEDCHILD")
    private short isunorganizedchild;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ISWORKING")
    private short isworking;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ISBOMZH")
    private short isbomzh;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ISSTUDENT")
    private short isstudent;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ISPENSIONER")
    private short ispensioner;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ISSOLDIER")
    private short issoldier;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ISSOLDIERFAMILYMEMBER")
    private short issoldierfamilymember;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ISBUDGETWORKER")
    private short isbudgetworker;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ISHARDWORK")
    private short ishardwork;
    @Size(max = 50)
    @Column(name = "PROFESSION")
    private String profession;
    @Size(max = 100)
    @Column(name = "WORKPOSITION")
    private String workposition;
    @Column(name = "FIREDATE")
    @Temporal(TemporalType.DATE)
    private Date firedate;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ISFROMDISTRICT")
    private short isfromdistrict;
    @Size(max = 255)
    @Column(name = "WORKPLACE")
    private String workplace;
    @Column(name = "ARDOCTORID")
    private Integer ardoctorid;
    @Column(name = "ARPATIENTID")
    private Integer arpatientid;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ISANONYM")
    private short isanonym;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "refpersons")
    private Collection<Hivpcr> hivpcrCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "refpersons")
    private Collection<Hivart> hivartCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "refpersons")
    private Collection<Hivstages> hivstagesCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "refpersons")
    private Collection<Hivimmunograms> hivimmunogramsCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "refpersons")
    private Collection<Hiv> hivCollection;
    @OneToMany(mappedBy = "refperson")
    private Collection<WebusersLog> webusersLogCollection;

    public Persons() {
    }

    public Persons(Integer id) {
        this.id = id;
    }

    public Persons(Integer id, String name, String nameI, short isdisabilityremoved, short istreatedasmandatoryinsured, short issocialservicesabandoned, short isorganizedchild, short isunorganizedchild, short isworking, short isbomzh, short isstudent, short ispensioner, short issoldier, short issoldierfamilymember, short isbudgetworker, short ishardwork, short isfromdistrict, short isanonym) {
        this.id = id;
        this.name = name;
        this.nameI = nameI;
        this.isdisabilityremoved = isdisabilityremoved;
        this.istreatedasmandatoryinsured = istreatedasmandatoryinsured;
        this.issocialservicesabandoned = issocialservicesabandoned;
        this.isorganizedchild = isorganizedchild;
        this.isunorganizedchild = isunorganizedchild;
        this.isworking = isworking;
        this.isbomzh = isbomzh;
        this.isstudent = isstudent;
        this.ispensioner = ispensioner;
        this.issoldier = issoldier;
        this.issoldierfamilymember = issoldierfamilymember;
        this.isbudgetworker = isbudgetworker;
        this.ishardwork = ishardwork;
        this.isfromdistrict = isfromdistrict;
        this.isanonym = isanonym;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNameI() {
        return nameI;
    }

    public void setNameI(String nameI) {
        this.nameI = nameI;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public Date getDateofdeath() {
        return dateofdeath;
    }

    public void setDateofdeath(Date dateofdeath) {
        this.dateofdeath = dateofdeath;
    }

    public String getAmbulatorycardno() {
        return ambulatorycardno;
    }

    public void setAmbulatorycardno(String ambulatorycardno) {
        this.ambulatorycardno = ambulatorycardno;
    }

    public String getSocialno() {
        return socialno;
    }

    public void setSocialno(String socialno) {
        this.socialno = socialno;
    }

    public String getPersonalno() {
        return personalno;
    }

    public void setPersonalno(String personalno) {
        this.personalno = personalno;
    }

    public String getPersonalcertificateno() {
        return personalcertificateno;
    }

    public void setPersonalcertificateno(String personalcertificateno) {
        this.personalcertificateno = personalcertificateno;
    }

    public String getHomephoneno() {
        return homephoneno;
    }

    public void setHomephoneno(String homephoneno) {
        this.homephoneno = homephoneno;
    }

    public String getWorkphoneno() {
        return workphoneno;
    }

    public void setWorkphoneno(String workphoneno) {
        this.workphoneno = workphoneno;
    }

    public String getMobilephoneno() {
        return mobilephoneno;
    }

    public void setMobilephoneno(String mobilephoneno) {
        this.mobilephoneno = mobilephoneno;
    }

    public String getHousenoReg() {
        return housenoReg;
    }

    public void setHousenoReg(String housenoReg) {
        this.housenoReg = housenoReg;
    }

    public String getBuildingReg() {
        return buildingReg;
    }

    public void setBuildingReg(String buildingReg) {
        this.buildingReg = buildingReg;
    }

    public String getRoomnoReg() {
        return roomnoReg;
    }

    public void setRoomnoReg(String roomnoReg) {
        this.roomnoReg = roomnoReg;
    }

    public String getNoteforaddressReg() {
        return noteforaddressReg;
    }

    public void setNoteforaddressReg(String noteforaddressReg) {
        this.noteforaddressReg = noteforaddressReg;
    }

    public String getAddressReg() {
        return addressReg;
    }

    public void setAddressReg(String addressReg) {
        this.addressReg = addressReg;
    }

    public String getHousenoLiv() {
        return housenoLiv;
    }

    public void setHousenoLiv(String housenoLiv) {
        this.housenoLiv = housenoLiv;
    }

    public String getBuildingLiv() {
        return buildingLiv;
    }

    public void setBuildingLiv(String buildingLiv) {
        this.buildingLiv = buildingLiv;
    }

    public String getRoomnoLiv() {
        return roomnoLiv;
    }

    public void setRoomnoLiv(String roomnoLiv) {
        this.roomnoLiv = roomnoLiv;
    }

    public String getNoteforaddressLiv() {
        return noteforaddressLiv;
    }

    public void setNoteforaddressLiv(String noteforaddressLiv) {
        this.noteforaddressLiv = noteforaddressLiv;
    }

    public String getAddressLiv() {
        return addressLiv;
    }

    public void setAddressLiv(String addressLiv) {
        this.addressLiv = addressLiv;
    }

    public String getDocumentno() {
        return documentno;
    }

    public void setDocumentno(String documentno) {
        this.documentno = documentno;
    }

    public String getDocdeliver() {
        return docdeliver;
    }

    public void setDocdeliver(String docdeliver) {
        this.docdeliver = docdeliver;
    }

    public Date getDeliverydate() {
        return deliverydate;
    }

    public void setDeliverydate(Date deliverydate) {
        this.deliverydate = deliverydate;
    }

    public short getIsdisabilityremoved() {
        return isdisabilityremoved;
    }

    public void setIsdisabilityremoved(short isdisabilityremoved) {
        this.isdisabilityremoved = isdisabilityremoved;
    }

    public short getIstreatedasmandatoryinsured() {
        return istreatedasmandatoryinsured;
    }

    public void setIstreatedasmandatoryinsured(short istreatedasmandatoryinsured) {
        this.istreatedasmandatoryinsured = istreatedasmandatoryinsured;
    }

    public short getIssocialservicesabandoned() {
        return issocialservicesabandoned;
    }

    public void setIssocialservicesabandoned(short issocialservicesabandoned) {
        this.issocialservicesabandoned = issocialservicesabandoned;
    }

    public short getIsorganizedchild() {
        return isorganizedchild;
    }

    public void setIsorganizedchild(short isorganizedchild) {
        this.isorganizedchild = isorganizedchild;
    }

    public short getIsunorganizedchild() {
        return isunorganizedchild;
    }

    public void setIsunorganizedchild(short isunorganizedchild) {
        this.isunorganizedchild = isunorganizedchild;
    }

    public short getIsworking() {
        return isworking;
    }

    public void setIsworking(short isworking) {
        this.isworking = isworking;
    }

    public short getIsbomzh() {
        return isbomzh;
    }

    public void setIsbomzh(short isbomzh) {
        this.isbomzh = isbomzh;
    }

    public short getIsstudent() {
        return isstudent;
    }

    public void setIsstudent(short isstudent) {
        this.isstudent = isstudent;
    }

    public short getIspensioner() {
        return ispensioner;
    }

    public void setIspensioner(short ispensioner) {
        this.ispensioner = ispensioner;
    }

    public short getIssoldier() {
        return issoldier;
    }

    public void setIssoldier(short issoldier) {
        this.issoldier = issoldier;
    }

    public short getIssoldierfamilymember() {
        return issoldierfamilymember;
    }

    public void setIssoldierfamilymember(short issoldierfamilymember) {
        this.issoldierfamilymember = issoldierfamilymember;
    }

    public short getIsbudgetworker() {
        return isbudgetworker;
    }

    public void setIsbudgetworker(short isbudgetworker) {
        this.isbudgetworker = isbudgetworker;
    }

    public short getIshardwork() {
        return ishardwork;
    }

    public void setIshardwork(short ishardwork) {
        this.ishardwork = ishardwork;
    }

    public String getProfession() {
        return profession;
    }

    public void setProfession(String profession) {
        this.profession = profession;
    }

    public String getWorkposition() {
        return workposition;
    }

    public void setWorkposition(String workposition) {
        this.workposition = workposition;
    }

    public Date getFiredate() {
        return firedate;
    }

    public void setFiredate(Date firedate) {
        this.firedate = firedate;
    }

    public short getIsfromdistrict() {
        return isfromdistrict;
    }

    public void setIsfromdistrict(short isfromdistrict) {
        this.isfromdistrict = isfromdistrict;
    }

    public String getWorkplace() {
        return workplace;
    }

    public void setWorkplace(String workplace) {
        this.workplace = workplace;
    }

    public Integer getArdoctorid() {
        return ardoctorid;
    }

    public void setArdoctorid(Integer ardoctorid) {
        this.ardoctorid = ardoctorid;
    }

    public Integer getArpatientid() {
        return arpatientid;
    }

    public void setArpatientid(Integer arpatientid) {
        this.arpatientid = arpatientid;
    }

    public short getIsanonym() {
        return isanonym;
    }

    public void setIsanonym(short isanonym) {
        this.isanonym = isanonym;
    }

    @XmlTransient
    public Collection<Hivpcr> getHivpcrCollection() {
        return hivpcrCollection;
    }

    public void setHivpcrCollection(Collection<Hivpcr> hivpcrCollection) {
        this.hivpcrCollection = hivpcrCollection;
    }

    @XmlTransient
    public Collection<Hivart> getHivartCollection() {
        return hivartCollection;
    }

    public void setHivartCollection(Collection<Hivart> hivartCollection) {
        this.hivartCollection = hivartCollection;
    }

    @XmlTransient
    public Collection<Hivstages> getHivstagesCollection() {
        return hivstagesCollection;
    }

    public void setHivstagesCollection(Collection<Hivstages> hivstagesCollection) {
        this.hivstagesCollection = hivstagesCollection;
    }

    @XmlTransient
    public Collection<Hivimmunograms> getHivimmunogramsCollection() {
        return hivimmunogramsCollection;
    }

    public void setHivimmunogramsCollection(Collection<Hivimmunograms> hivimmunogramsCollection) {
        this.hivimmunogramsCollection = hivimmunogramsCollection;
    }

    @XmlTransient
    public Collection<Hiv> getHivCollection() {
        return hivCollection;
    }

    public void setHivCollection(Collection<Hiv> hivCollection) {
        this.hivCollection = hivCollection;
    }

    @XmlTransient
    public Collection<WebusersLog> getWebusersLogCollection() {
        return webusersLogCollection;
    }

    public void setWebusersLogCollection(Collection<WebusersLog> webusersLogCollection) {
        this.webusersLogCollection = webusersLogCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Persons)) {
            return false;
        }
        Persons other = (Persons) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.Persons[ id=" + id + " ]";
    }

}
