/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.initmed.aidsbaseview.entity;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author sd199
 */
@Entity
@Table(name = "HIVANALYSISRESULTS")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Hivanalysisresults.findAll", query = "SELECT h FROM Hivanalysisresults h")
    , @NamedQuery(name = "Hivanalysisresults.findById", query = "SELECT h FROM Hivanalysisresults h WHERE h.id = :id")
    , @NamedQuery(name = "Hivanalysisresults.findByName", query = "SELECT h FROM Hivanalysisresults h WHERE h.name = :name")
    , @NamedQuery(name = "Hivanalysisresults.findByShortname", query = "SELECT h FROM Hivanalysisresults h WHERE h.shortname = :shortname")})
public class Hivanalysisresults implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "NAME")
    private String name;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "SHORTNAME")
    private String shortname;
    @OneToMany(mappedBy = "refhivanalysisresults")
    private Collection<Hivpcr> hivpcrCollection;
    @JoinColumn(name = "ATYPE", referencedColumnName = "ID")
    @ManyToOne
    private Hivanalysistypes atype;

    public Hivanalysisresults() {
    }

    public Hivanalysisresults(Integer id) {
        this.id = id;
    }

    public Hivanalysisresults(Integer id, String name, String shortname) {
        this.id = id;
        this.name = name;
        this.shortname = shortname;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getShortname() {
        return shortname;
    }

    public void setShortname(String shortname) {
        this.shortname = shortname;
    }

    @XmlTransient
    public Collection<Hivpcr> getHivpcrCollection() {
        return hivpcrCollection;
    }

    public void setHivpcrCollection(Collection<Hivpcr> hivpcrCollection) {
        this.hivpcrCollection = hivpcrCollection;
    }

    public Hivanalysistypes getAtype() {
        return atype;
    }

    public void setAtype(Hivanalysistypes atype) {
        this.atype = atype;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Hivanalysisresults)) {
            return false;
        }
        Hivanalysisresults other = (Hivanalysisresults) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.Hivanalysisresults[ id=" + id + " ]";
    }
    
}
