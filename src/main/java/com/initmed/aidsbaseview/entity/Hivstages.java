/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.initmed.aidsbaseview.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author sd199
 */
@Entity
@Table(name = "HIVSTAGES")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Hivstages.findAll", query = "SELECT h FROM Hivstages h")
    , @NamedQuery(name = "Hivstages.findById", query = "SELECT h FROM Hivstages h WHERE h.id = :id")
    , @NamedQuery(name = "Hivstages.findByStartdate", query = "SELECT h FROM Hivstages h WHERE h.startdate = :startdate")
    , @NamedQuery(name = "Hivstages.findByEnddate", query = "SELECT h FROM Hivstages h WHERE h.enddate = :enddate")})
public class Hivstages implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Column(name = "STARTDATE")
    @Temporal(TemporalType.DATE)
    private Date startdate;
    @Column(name = "ENDDATE")
    @Temporal(TemporalType.DATE)
    private Date enddate;
    @JoinColumn(name = "REFHIVPHASES", referencedColumnName = "ID")
    @ManyToOne
    private Hivphases refhivphases;
    @JoinColumn(name = "REFHIVSTAGETYPES", referencedColumnName = "ID")
    @ManyToOne(optional = false)
    private Hivstagetypes refhivstagetypes;
    @JoinColumn(name = "REFPERSONS", referencedColumnName = "ID")
    @ManyToOne(optional = false)
    private Persons refpersons;

    public Hivstages() {
    }

    public Hivstages(Integer id) {
        this.id = id;
    }

    public Hivstages(Integer id, Date startdate) {
        this.id = id;
        this.startdate = startdate;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getStartdate() {
        return startdate;
    }

    public void setStartdate(Date startdate) {
        this.startdate = startdate;
    }

    public Date getEnddate() {
        return enddate;
    }

    public void setEnddate(Date enddate) {
        this.enddate = enddate;
    }

    public Hivphases getRefhivphases() {
        return refhivphases;
    }

    public void setRefhivphases(Hivphases refhivphases) {
        this.refhivphases = refhivphases;
    }

    public Hivstagetypes getRefhivstagetypes() {
        return refhivstagetypes;
    }

    public void setRefhivstagetypes(Hivstagetypes refhivstagetypes) {
        this.refhivstagetypes = refhivstagetypes;
    }

    public Persons getRefpersons() {
        return refpersons;
    }

    public void setRefpersons(Persons refpersons) {
        this.refpersons = refpersons;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Hivstages)) {
            return false;
        }
        Hivstages other = (Hivstages) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.Hivstages[ id=" + id + " ]";
    }
    
}
