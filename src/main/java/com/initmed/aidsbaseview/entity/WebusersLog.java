/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.initmed.aidsbaseview.entity;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;

import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author sd199
 */
@Entity
@SequenceGenerator(
        name = "G_WEBUSERS_LOG",
        sequenceName = "G_WEBUSERS_LOG"
)
@Table(name = "WEBUSERS_LOG")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "WebusersLog.findAll", query = "SELECT w FROM WebusersLog w"),
    @NamedQuery(name = "WebusersLog.findById", query = "SELECT w FROM WebusersLog w WHERE w.id = :id"),
    @NamedQuery(name = "WebusersLog.findByWatchdate", query = "SELECT w FROM WebusersLog w WHERE w.watchdate = :watchdate")})
public class WebusersLog implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "G_WEBUSERS_LOG")
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Integer id;
    @Column(name = "WATCHDATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date watchdate;
    @JoinColumn(name = "REFPERSON", referencedColumnName = "ID")
    @ManyToOne
    private Persons refperson;
    @JoinColumn(name = "REFWEBUSER", referencedColumnName = "ID")
    @ManyToOne
    private Webusers refwebuser;

    public WebusersLog() {
    }

    public WebusersLog(Webusers user, Persons per) {
        this.refwebuser = user;
        this.refperson = per;
        this.watchdate = Calendar.getInstance().getTime();
    }

    public WebusersLog(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getWatchdate() {
        return watchdate;
    }

    public void setWatchdate(Date watchdate) {
        this.watchdate = watchdate;
    }

    public Persons getRefperson() {
        return refperson;
    }

    public void setRefperson(Persons refperson) {
        this.refperson = refperson;
    }

    public Webusers getRefwebuser() {
        return refwebuser;
    }

    public void setRefwebuser(Webusers refwebuser) {
        this.refwebuser = refwebuser;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof WebusersLog)) {
            return false;
        }
        WebusersLog other = (WebusersLog) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.WebusersLog[ id=" + id + " ]";
    }

}
