/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.initmed.aidsbaseview.entity;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author sd199
 */
@Entity
@Table(name = "WEBUSERS")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Webusers.findAll", query = "SELECT w FROM Webusers w"),
    @NamedQuery(name = "Webusers.findById", query = "SELECT w FROM Webusers w WHERE w.id = :id"),
    @NamedQuery(name = "Webusers.findByName", query = "SELECT w FROM Webusers w WHERE w.name = :name"),
    @NamedQuery(name = "Webusers.findByUsername", query = "SELECT w FROM Webusers w WHERE w.username = :username"),
    @NamedQuery(name = "Webusers.findByGroupname", query = "SELECT w FROM Webusers w WHERE w.groupname = :groupname"),
    @NamedQuery(name = "Webusers.findByDbpassword", query = "SELECT w FROM Webusers w WHERE w.dbpassword = :dbpassword")})
public class Webusers implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 80)
    @Column(name = "NAME")
    private String name;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 32)
    @Column(name = "USERNAME")
    private String username;
    @Size(max = 32)
    @Column(name = "GROUPNAME")
    private String groupname;
    @Size(max = 255)
    @Column(name = "DBPASSWORD")
    private String dbpassword;
    @OneToMany(mappedBy = "refwebuser")
    private Collection<WebusersLog> webusersLogCollection;

    public Webusers() {
    }

    public Webusers(Integer id) {
        this.id = id;
    }

    public Webusers(Integer id, String name, String username) {
        this.id = id;
        this.name = name;
        this.username = username;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getGroupname() {
        return groupname;
    }

    public void setGroupname(String groupname) {
        this.groupname = groupname;
    }

    public String getDbpassword() {
        return dbpassword;
    }

    public void setDbpassword(String dbpassword) {
        this.dbpassword = dbpassword;
    }

    @XmlTransient
    public Collection<WebusersLog> getWebusersLogCollection() {
        return webusersLogCollection;
    }

    public void setWebusersLogCollection(Collection<WebusersLog> webusersLogCollection) {
        this.webusersLogCollection = webusersLogCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Webusers)) {
            return false;
        }
        Webusers other = (Webusers) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.Webusers[ id=" + id + " ]";
    }

}
