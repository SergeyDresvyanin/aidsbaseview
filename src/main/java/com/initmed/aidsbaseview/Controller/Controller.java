/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.initmed.aidsbaseview.Controller;

import com.initmed.aidsbaseview.beans.HivFacade;
import com.initmed.aidsbaseview.beans.HivimmunogramsFacade;
import com.initmed.aidsbaseview.beans.PersonsFacade;
import com.initmed.aidsbaseview.beans.VPersonsDiagnosesFacade;
import com.initmed.aidsbaseview.beans.WebusersFacade;
import com.initmed.aidsbaseview.beans.WebusersLogFacade;
import com.initmed.aidsbaseview.entity.Hiv;
import com.initmed.aidsbaseview.entity.Hivimmunograms;
import com.initmed.aidsbaseview.entity.Persons;
import com.initmed.aidsbaseview.entity.VPersonsDiagnoses;
import com.initmed.aidsbaseview.entity.Webusers;
import com.initmed.aidsbaseview.entity.WebusersLog;
import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.text.ParseException;
import java.util.Base64;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.DataFormat;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

/**
 *
 * @author sd199
 */
public class Controller extends HttpServlet {

    @EJB
    private PersonsFacade personsFacade;
    @EJB
    private VPersonsDiagnosesFacade personsDiagnosesFacade;
    @EJB
    private WebusersFacade webusersFacade;
    @EJB
    private WebusersLogFacade webusersLogFacade;
    @EJB
    private HivFacade hivFacade;
    @EJB
    private HivimmunogramsFacade hivimmunogramsFacade;

    private String userPath;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

        /* TODO output your page here. You may use following sample code. */
        userPath = request.getServletPath();
        if (userPath.equals("/search")) {
            userPath = "/listPersonForm";
            handleSearchRequest(request, response);
        }
        if (userPath.equals("/downloadreport")) {
            handleDownloadReport(request, response);
        }
        if (userPath.equals("/aboutPerson")) {
            userPath = "/personInformation";
            handleAboutPersonRequest(request, response);
        }

        String url = "/WEB-INF/" + userPath + ".jsp";
        try {
            if (!response.isCommitted()) {
                request.getRequestDispatcher(url).forward(request, response);
            }
        } catch (IOException ex) {
            getServletContext().log(ex.getMessage());
        } catch (ServletException ex) {
            getServletContext().log(ex.getMessage());
        }
    }

    private void handleDownloadReport(HttpServletRequest request, HttpServletResponse response) {

        String filterName = request.getParameter("filterName");
        String placeLiv = request.getParameter("placeLive");

        String dateFrom = request.getParameter("datefrom");
        String dateTo = request.getParameter("dateto");

        String lowcd4 = request.getParameter("lowcd4");

        try {
            List<Persons> resultPersons = personsFacade.findByFilter(filterName, placeLiv, dateFrom, dateTo, lowcd4.equals("true"), 0, 9999);
            byte[] res = getExcellReport(resultPersons, dateFrom, dateTo, lowcd4.equals("true"));
            res = Base64.getEncoder().encode(res);
            response.setContentType("MIME type: application/excel");
            response.setContentLength(res.length);
            String headerKey = "Content-Disposition";
            String headerValue = String.format("attachment; filename=\"%s\"", "report.xls");
            response.setHeader(headerKey, headerValue);
            OutputStream outStream = response.getOutputStream();
            outStream.write(res);
            outStream.close();

        } catch (ParseException ex) {
            Logger.getLogger(Controller.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Controller.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void handleSearchRequest(HttpServletRequest request, HttpServletResponse response) {
        int pos = 0;
        int rowCount = 25;
        String start = request.getParameter("startPos");
        String idCode = request.getParameter("idCode");
        if (idCode != null && idCode.length() >= 1) {
            request.setAttribute("persons", personsFacade.findByIDCode(idCode));
        } else {
            if (start != null) {
                pos = Integer.parseInt(start);
            }
            String count = request.getParameter("count");
            if (count != null) {
                rowCount = Integer.parseInt(count);
            }
            String filterName = request.getParameter("filterName").trim();
            String placeLiv = request.getParameter("placeLive").trim();

            String dateFrom = request.getParameter("datefrom");
            String dateTo = request.getParameter("dateto");

            String lowcd4 = request.getParameter("lowcd4");

            List<Persons> resultPersons;
            try {
                resultPersons = personsFacade.findByFilter(filterName, placeLiv, dateFrom, dateTo, lowcd4.equals("true"), pos, rowCount);
                request.setAttribute("persons", resultPersons);
            } catch (ParseException ex) {
                Logger.getLogger(Controller.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public byte[] getExcellReport(List<Persons> personses, String from, String to, boolean lowcd4) throws FileNotFoundException, IOException {
        Workbook book = new HSSFWorkbook();
        Sheet sheet = book.createSheet("Patients");
        Row row = sheet.createRow(0);

        DataFormat format = book.createDataFormat();
        CellStyle dateStyle = book.createCellStyle();
        dateStyle.setDataFormat(format.getFormat("dd.mm.yyyy"));

        row.createCell(0).setCellValue("ФИО");
        row.createCell(1).setCellValue("Дата рождения");
        row.createCell(2).setCellValue("Адрес");
        row.createCell(3).setCellValue("СНИЛС");
        row.createCell(4).setCellValue("Регистрационный номер");
        row.createCell(5).setCellValue("Дата CD4");
        row.createCell(6).setCellValue("CD4");

        int i = 1;
        for (Persons ps : personses) {
            Row r = sheet.createRow(i);
            r.createCell(0).setCellValue(ps.getName());

            Cell bDate = r.createCell(1);
            if (ps.getBirthday() != null) {
                bDate.setCellValue(ps.getBirthday());
            }
            bDate.setCellStyle(dateStyle);                        

            r.createCell(2).setCellValue(ps.getAddressLiv());
            r.createCell(3).setCellValue(ps.getSocialno());
            r.createCell(4).setCellValue(ps.getAmbulatorycardno());
            try {
                List<Hivimmunograms> hivimmunograms = hivimmunogramsFacade.getLastCd4(from, to, lowcd4, ps);
                if (hivimmunograms != null && !hivimmunograms.isEmpty()) {

                    Cell c = r.createCell(5);
                    c.setCellValue(hivimmunograms.get(0).getTakingdate());
                    c.setCellStyle(dateStyle);

                    r.createCell(6).setCellValue(hivimmunograms.get(0).getCd4());
                }
            } catch (ParseException ex) {
                Logger.getLogger(Controller.class.getName()).log(Level.SEVERE, null, ex);
            }

            i++;
        }

        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        try {
            book.write(bos);
        } finally {
            bos.close();
        }
        return bos.toByteArray();
    }

    private void handleAboutPersonRequest(HttpServletRequest request, HttpServletResponse response) {
        int id = Integer.parseInt(request.getParameter("id"));
        String userLogin = request.getRemoteUser();
        Webusers webUser = webusersFacade.getUser(userLogin);
        Persons resultPersons = personsFacade.getPersonByID(id);
        WebusersLog logRow = new WebusersLog(webUser, resultPersons);
        webusersLogFacade.create(logRow);
        List<VPersonsDiagnoses> resDiagnose = personsDiagnosesFacade.getByID(id);
        if (resultPersons != null) {
            request.setAttribute("currentPerson", resultPersons);
            request.setAttribute("perDiagnos", resDiagnose);
        }
    }

// <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }
}
