var startPos = 0;
var rowCount = 7;
var canLoad = true;
var foundNothing = ('<tr><td  colspan="3" ><font size="10">Ничего не найдено</font></td></tr>');

$(function () {
    $('#datefrom').datepicker({
        dateFormat: 'yy-mm-dd',
        onSelect: function (dateText) {
            $('#idCode').val('');
            ajaxSearch({idCode: $('#idCode').val(), filterName: $('#personName').val(), placeLive: $('#cityLiv').val(), datefrom: $('#datefrom').val(), dateto: $('#dateto').val(), lowcd4: $('#lowcd4').is(":checked"), startPos: 0});
        }
    });
    $('#dateto').datepicker({
        dateFormat: 'yy-mm-dd',
        onSelect: function (dateText) {
            $('#idCode').val('');

            ajaxSearch({idCode: $('#idCode').val(), filterName: $('#personName').val(), placeLive: $('#cityLiv').val(), datefrom: $('#datefrom').val(), dateto: $('#dateto').val(), lowcd4: $('#lowcd4').is(":checked"), startPos: 0});
        }
    });

    $('#personName').autocomplete({
        source: function () {
            $('#idCode').val('');
            $('#dateto').val('');
            $('#datefrom').val('');
            ajaxSearch({idCode: $('#idCode').val(), filterName: $('#personName').val(), placeLive: $('#cityLiv').val(), datefrom: $('#datefrom').val(), dateto: $('#dateto').val(), lowcd4: $('#lowcd4').is(":checked"), startPos: 0});
        },
        minLength: 2
    });
    $('#cityLiv').autocomplete({
        source: function () {
            $('#idCode').val('');
            $('#dateto').val('');
            $('#datefrom').val('');
            ajaxSearch({idCode: $('#idCode').val(), filterName: $('#personName').val(), placeLive: $('#cityLiv').val(), datefrom: $('#datefrom').val(), dateto: $('#dateto').val(), lowcd4: $('#lowcd4').is(":checked"), startPos: 0});
        },
        minLength: 2
    });
    $('#idCode').autocomplete({
        source: function () {
            $('#personName').val('');
            $('#cityLiv').val('');
            ajaxSearch({idCode: $('#idCode').val(), filterName: $('#personName').val(), placeLive: $('#cityLiv').val(), datefrom: $('#datefrom').val(), dateto: $('#dateto').val(), lowcd4: $('#lowcd4').is(":checked"), startPos: 0});
        }
    });

    $('#lowcd4').autocomplete({
        source: function () {
            ajaxSearch({idCode: $('#idCode').val(), filterName: $('#personName').val(), placeLive: $('#cityLiv').val(), datefrom: $('#datefrom').val(), dateto: $('#dateto').val(), lowcd4: $('#lowcd4').is(":checked"), startPos: 0});
        },
        minLength: 2
    });

});

function downloadReport() {
    $.post({
        url: 'downloadreport',
        data: {filterName: $('#personName').val(), placeLive: $('#cityLiv').val(), datefrom: $('#datefrom').val(), dateto: $('#dateto').val(), lowcd4: $('#lowcd4').is(":checked")},
        success: function (data) {



            var blob = new Blob([s2ab(atob(data))], {type: "image/excel"});


            if (window.navigator && window.navigator.msSaveOrOpenBlob) {
                window.navigator.msSaveOrOpenBlob(blob);
                return;
            }

            var link = document.createElement('a');
            var filename = 'report.xls'
            link.href = window.URL.createObjectURL(blob);
            link.download = filename;

            document.body.appendChild(link);

            link.click();

            document.body.removeChild(link);
        }
    });
}
;

function s2ab(s) {
    var buf = new ArrayBuffer(s.length);
    var view = new Uint8Array(buf);
    for (var i = 0; i != s.length; ++i)
        view[i] = s.charCodeAt(i) & 0xFF;
    return buf;
}

function ajaxSearch(data) {
    startPos = 0;
    $.post({
        url: "search",
        data: data,
        dataType: "html",
        success: function (data) {
            if (data.indexOf('<tr') === -1)
            {
                $('#listPerson').html(foundNothing);
            } else {
                $('#listPerson').html(data);
                startPos = startPos + 25;
                $('#listPerson').highlight($('#personName').val());
                $('#listPerson').highlight($('#cityLiv').val());
            }
        }
    });
}
$(document).ready(function () {
    $(window).scroll(function () {
        if (canLoad === true && ($(window).scrollTop() > $('body').height() - $(window).height() - 10)) {
            canLoad = false;
            $.post({
                url: "search",
                data: {idCode: $('#idCode').val(), filterName: $('#personName').val(), placeLive: $('#cityLiv').val(), datefrom: $('#datefrom').val(), dateto: $('#dateto').val(), lowcd4: $('#lowcd4').is(":checked"), startPos: startPos, count: rowCount},
                dataType: "html",
                success: function (data) {
                    $('#listPerson').append(data);
                    canLoad = true;
                    startPos = startPos + rowCount;
                    $('#listPerson').highlight($('#personName').val());
                    $('#listPerson').highlight($('#cityLiv').val());

                }
            });
        }
    });
});

function aboutPerson(id) {
    if (canLoad) {
        canLoad = false;
        $.ajax({
            url: 'aboutPerson',
            data: {id: id},
            success: function (data) {
                personView("Информация о пациенте", data);
                canLoad = true;
            }
        });
    }
}

function personView(title, text) {
    $('body').css('overflow', 'hidden');
    var dWidth = $(window).width() * 0.9;
    var dHeight = $(window).height() * 0.9;

    var dial = $('<div></div>').html(text).dialog({
        title: title,
        modal: true,
        width: dWidth,
        height: dHeight,
        open: function () {
            canLoad = false;
        },
        close: function () {
            $('body').css('overflow', 'visible');
            canLoad = true;
            $(this).dialog('destroy').remove();
        }
    });
}
