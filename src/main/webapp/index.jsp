<%-- 
    Document   : index
    Created on : 16.03.2017, 20:26:27
    Author     : Sergey_Dresvyanin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" href="css/style.css">
        <link rel="stylesheet" type="text/css" href="css/jquery-ui.css">

        <link rel="stylesheet" type="text/css" href="css/jquery-ui.css">
        <title>Просмотр пациентов центр СПИД</title>
        <script src="jquery-1.12.4.js"></script>
        <script src="jquery-ui.js"></script>
        <script src="scripts.js"></script>
        <script src="jquery.highlight-5.js"></script>
    </head>
    <body>
        <%@include file="/jspf/header.jspf" %>
        <div id="searchform" method="POST" class="search" style='text-align: center;'>
            <h2>Фильтр по данным пациента</h2>
            <input class="search-box" id="personName" type="search" name="input" placeholder= "Поиск по ФИО"/>
            <input class="search-box" id="cityLiv" type="search" name="input" placeholder= "Адрес"/>
            <input class="search-box" id="idCode" type="search" name="input" placeholder= "Регистрационный номер"/>
        </div>
        <div id="searchform" method="POST" class="search" style='text-align: center;'>
            <h2>Фильтр по дата регистрации</h2>
            <input class="search-box" id="datefrom"  placeholder= "Дата с:" />
            <input class="search-box" id="dateto"   placeholder= "Дата по:"/>
            <label for="lowcd4" style="font-size: 14pt;" >отоброжать только CD4<350</label>
            <input  id="lowcd4" style="font-size: 10pt; height: 20px;" type="checkbox" checked="false"/>
        </div>
        <div id="searchform" method="POST" class="search" style='text-align: center; margin-top: 3em'>
            <input id="btndownload" class="search-box" type="button" onClick="downloadReport()" value="Скачать список"/>
        </div>
        <form>
            <table  align="center" class="persons-table" >    
                <thead>
                    <tr>
                        <th width="10%">ФИО</th>
                        <th width="3%">Дата рождения</th>
                        <th width="13%">Адрес</th>
                        <th width="13%">Рег. номер</th>
                    </tr>
                </thead>
                <tbody id="listPerson">
                </tbody>
            </table>
        </form>
    </body>
</html>