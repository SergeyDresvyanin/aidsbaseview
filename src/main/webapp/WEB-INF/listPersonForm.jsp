<%-- 
    Document   : listPersonForm
    Created on : 19.03.2017, 8:42:50
    Author     : Sergey_Dresvyanin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%><!DOCTYPE html>


<c:forEach var="person" items="${persons}">
    <tr onclick="aboutPerson(${person.id})">
        <td><c:out value="${person.name}"/></td>
        <c:if test="${person.addressLiv==null}">
        </c:if>
        <td><fmt:formatDate type="date" value="${person.birthday}"/></td>
        <c:choose>
            <c:when test="${person.addressLiv!=null}">
                <td><c:out value="${person.addressLiv}"/></td>
            </c:when>    
            <c:otherwise>
                <td><c:out value="${person.addressReg}"/></td>
            </c:otherwise>
        </c:choose>
        <td><c:out value="${person.ambulatorycardno}"/></td>
    </tr>
</c:forEach>