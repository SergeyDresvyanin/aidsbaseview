<%-- 
    Document   : personInfromation
    Created on : 16.03.2017, 13:39:14
    Author     : sd199
--%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" type="text/css" href="css/style.css">
        <link rel="stylesheet" type="text/css" href="css/jquery-ui.css">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Данные о <c:out value="${currentPerson.name}"/> пациенте</title>
    </head>
    <body>
        <div id="personInformation">
            <h1 class="h1-syle"><c:out value="${currentPerson.name}"/></h1>
            <table class="persons-table">
                <thead>
                    <tr>
                        <th>Место регистрации</th>
                        <th>Место жительства</th>
                        <th>Дата рождения</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td><c:out value="${currentPerson.addressReg}"/></td>
                        <td><c:out value="${currentPerson.addressLiv}"/></td>
                        <td><fmt:formatDate type="date" value="${currentPerson.birthday}"/></td>
                    </tr>
                </tbody>
            </table>
            <table class="persons-table">
                <thead>
                    <tr>
                        <th>Дата определния</th>
                        <th>Путь заражения</th>
                    </tr>
                </thead>
                <tbody>
                    <c:forEach var="c" items="${currentPerson.hivCollection}">
                        <tr>
                            <td><fmt:formatDate type="date" value="${c.detectiondate}"/></td>
                            <td><c:out value="${c.refhivinfectionways.name}"/></td>
                        </tr>
                    </c:forEach>
                </tbody>
            </table>
            <h2  class="h2-syle">Иммунограмма</h2>
            <table class="persons-table">
                <thead>
                    <tr>
                        <th>Дата забора</th>
                        <th>Дата выполнения</th>
                        <th>Дата ответа</th>
                        <th>CD 4</th>
                        <th>CD 4%</th>
                        <th>CD 8</th>
                        <th>CD 8%</th>
                        <th>CD 3</th>
                        <th>CD 3%</th>
                        <th>ИРИ</th>
                    </tr>
                </thead>
                <tbody>
                    <c:forEach var="immun" items="${currentPerson.hivimmunogramsCollection}">
                        <tr>
                            <td><fmt:formatDate type="date" value="${immun.takingdate}"/></td>
                            <td><fmt:formatDate type="date" value="${immun.makingdate}"/></td>
                            <td><fmt:formatDate type="date" value="${immun.answerdate}"/></td>
                            <td><c:out value="${immun.cd4}"/></td>
                            <td><c:out value="${immun.cd4Percent}"/></td>
                            <td><c:out value="${immun.cd8}"/></td>
                            <td><c:out value="${immun.cd8Percent}"/></td>
                            <td><c:out value="${immun.cd3}"/></td>
                            <td><c:out value="${immun.cd3Percent}"/></td>
                            <td><c:out value="${immun.iri}"/></td>
                        </tr>
                    </c:forEach>
                </tbody>
            </table>
            <h2 class="h2-syle">Стадии ВИЧ</h2>
            <table class="persons-table">
                <thead>
                    <tr>
                        <th>Дата</th>
                        <th>Стадия ВИЧ</th>
                        <th>Фаза</th>
                    </tr>
                </thead>
                <tbody>
                    <c:forEach var="stage" items="${currentPerson.hivstagesCollection}">
                        <tr>
                            <td><fmt:formatDate type="date" value="${stage.startdate}"/></td>
                            <td><c:out value="${stage.refhivstagetypes.name}"/></td>
                            <td><c:out value="${stage.refhivphases.name}"/></td>
                        </tr>
                    </c:forEach>
                </tbody>
            </table>
            <h2 class="h2-syle">АРВТ</h2>
            <table class="persons-table">
                <thead>
                    <tr>
                        <th>Дата начала</th>
                        <th>Дата конца</th>
                        <th>Причина назначения</th>
                        <th>Причина отказа</th>
                        <th>Исход</th>
                        <th>Препарат</th>
                        <th>Схема примененеия</th>

                    </tr>
                </thead>
                <tbody>
                    <c:forEach var="hivArtPer" items="${currentPerson.hivartCollection}">
                    <td><fmt:formatDate type="date" value="${hivArtPer.startdate}"/></td>
                    <td><fmt:formatDate type="date" value="${hivArtPer.enddate}"/></td>
                    <td><c:out value="${hivArtPer.startreason}"/></td>
                    <td><c:out value="${hivArtPer.endreason}"/></td>
                    <td><c:out value="${hivArtPer.refhivartoutcomes.name}"/></td>
                    <td><c:forEach var="drug" items="${hivArtPer.hivartdrugsCollection}">
                            <div><c:out value="${drug.refdrugs.descr}"/></div>
                            <div><c:out value="${drug.description}"/></div>
                        </c:forEach>
                    </td>
                </c:forEach>
                </tbody>
            </table>
            <h2  class="h2-syle">Результаты ПЦР на ВИЧ</h2>
            <table class="persons-table">
                <thead>
                    <tr>
                        <th>Дата забора</th>
                        <th>Дата выполнения</th>
                        <th>Дата ответа</th>
                        <th>Результат</th>
                        <th>Вирусная нагрузка</th>
                    </tr>
                </thead>
                <tbody>
                    <c:forEach var="pcrInf" items="${currentPerson.hivpcrCollection}">
                        <tr>
                            <td><fmt:formatDate type="date" value="${pcrInf.takingdate}"/></td>
                            <td><fmt:formatDate type="date" value="${pcrInf.makingdate}"/></td>
                            <td><fmt:formatDate type="date" value="${pcrInf.answerdate}"/></td>
                            <td><c:out value="${pcrInf.refhivanalysisresults.name}"/></td>
                            <td><c:out value="${pcrInf.viralload}"/></td>
                        </tr>
                    </c:forEach>
                </tbody>
            </table>

            <h2  class="h2-syle">Сопутствующие заболевания</h2>
            <table class="persons-table">
                <thead>
                    <tr>
                        <th>Дата</th>
                        <th>Код</th>
                        <th>Заболевание</th>
                    </tr>
                </thead>
                <tbody>
                    <c:forEach var="diag" items="${perDiagnos}">
                        <tr>
                            <td><fmt:formatDate type="date" value="${diag.lastdate}"/></td>
                            <td><c:out value="${diag.classifiercode}"/></td>
                            <td><c:out value="${diag.name}"/></td>
                        </tr>
                    </tbody>
                </c:forEach>
            </table>
            <div id="progressbar"></div>
        </div>
    </body>
</html>