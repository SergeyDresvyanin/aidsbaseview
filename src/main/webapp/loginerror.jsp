<%-- 
    Document   : logout
    Created on : 21.08.2010, 17:42:47
    Author     : Григорий
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Веб-модуль inIT-TB: Авторизация</title>
        <link rel="stylesheet" type="text/css" href="style.css"/>
        <style type="text/css">
            body, html { font-size:120%; }
        </style>
    </head>
    <body class="claro">
        <br />
        <center>
         <h2>Вам не удалось войти в систему: неправильное имя пользователя или пароль!</h2>
         <br /><br />
            <a href="<c:url value="login.jsp"/>">Повторить ввод имени пользователя и пароля</a>
        </center>
    </body>
</html>
