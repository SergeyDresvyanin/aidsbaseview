<%-- 
    Document   : login
    Created on : 21.08.2010, 17:31:04
    Author     : Григорий
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Веб-модуль АС Инит-МЕД: Авторизация</title>
        <style type="text/css">
            body, html { font-size:120%; }
        </style>
    </head>
    <body class="claro">
        <center><br/><br/><br/>       
            <h2>Веб модуль автоматизированной системы Инит-МЕД приветствует Вас!</h2>
            <br/>
            <p>Для того, чтобы начать работу, Вам необходимо ввести имя пользователя и пароль</p>
            <form action="<c:url value="j_security_check"/>">
                <table width="80%">
                    <tr>
                        <td width="40%" valign="middle" align="center">
                            Имя пользователя:
                        </td>
                        <td width="60%" valign="middle" align="center">
                            <input type="text" name="j_username" id="j_username" tabindex="1" style="width: 100%"/>
                        </td>
                    </tr>
                    <tr>
                        <td width="40%" valign="middle" align="center">
                            Пароль:
                        </td>
                        <td width="60%" valign="middle" align="center">
                            <input type="password" name="j_password" id="j_password" tabindex="3" style="width: 100%"/>
                        </td>
                    </tr>
                    <tr>
                        <td width="100%" colspan="2" valign="middle" align="center">
                            <button type="submit" id="btnOK">ОК</button>
                        </td>
                    </tr>
                </table>
            </form>
        </center>
    </body>
</html>